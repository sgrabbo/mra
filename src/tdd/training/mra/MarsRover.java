package tdd.training.mra;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class MarsRover {
	
	protected int[][] planet;
	protected List <String> planetObstacles;
	protected int coordinateX, coordinateY;
	protected char direction;
	
	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		
		this.coordinateX = 0;
		this.coordinateY = 0;
		this.direction = 'N';
		this.planet = new int[planetX][planetY];
		this.planetObstacles = new ArrayList<> (planetObstacles);
		
		if (!this.checkPlanetObstacles()) {
			
			throw new MarsRoverException("The position of the planet's obstacles exceeds the dimension of the planet."); 
			
		}
		
	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {
		
		if (planetObstacles.contains("(".concat(String.valueOf(x).concat(",".concat(String.valueOf(y).concat(")")))))) {
			
			return true;
			
		} else {
			
			return false;
			
		}
		
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {
		
		if (commandString.length() == 0) {
			
			return "(0,0,N)";
			
		} else if (Pattern.compile("[fbrl]+$", Pattern.CASE_INSENSITIVE).matcher(commandString).find()) {
			
			String encounteredObstacles = "";
			int tempY, tempX;
			
			for (int i = 0; i < commandString.length(); i++) {
				
				switch (commandString.charAt(i)) {
				
					case 'r':
						
						if (this.direction == 'N') {
							
							this.direction = 'E';
							
						} else if (this.direction == 'E') {
							
							this.direction = 'S';
							
						} else if (this.direction == 'S') {
							
							this.direction = 'W';
							
						} else if (this.direction == 'W') {
							
							this.direction = 'N';
							
						}
						
						break;
						
					case 'l':
						
						if (this.direction == 'N') {
							
							this.direction = 'W';
							
						} else if (this.direction == 'E') {
							
							this.direction = 'N';
							
						} else if (this.direction == 'S') {
							
							this.direction = 'E';
							
						} else if (this.direction == 'W') {
							
							this.direction = 'S';
							
						}
						
						break;
						
					case 'f':
						
						if (this.direction == 'N') {
							
							encounteredObstacles = this.increasingY(encounteredObstacles);
							
						} else if (this.direction == 'E') {
							
							encounteredObstacles = this.increasingX(encounteredObstacles);
							
						} else if (this.direction == 'S') {
							
							encounteredObstacles = this.decreasingY(encounteredObstacles);
								
						} else if (this.direction == 'W') {
							
							encounteredObstacles = this.decreasingX(encounteredObstacles);
							
						}
						
						break;
						
					case 'b':
						
						if (this.direction == 'N') {
							
							encounteredObstacles = this.decreasingY(encounteredObstacles);
							
						} else if (this.direction == 'E') {
							
							encounteredObstacles = this.decreasingX(encounteredObstacles);
							
						} else if (this.direction == 'S') {
							
							encounteredObstacles = this.increasingY(encounteredObstacles);
							
						} else if (this.direction == 'W') {
							
							encounteredObstacles = this.increasingX(encounteredObstacles);
							
						}
						
						break;
						
					}
					
				}
			
			return "(".concat(String.valueOf(this.coordinateX).concat(",".concat(String.valueOf(this.coordinateY).concat(",".concat(String.valueOf(this.direction)).concat(")".concat(encounteredObstacles))))));
			
		} else {
			
			throw new MarsRoverException("Command not recognized.");
			
		}
		
	}
	
	/**
	 * It check if the quadrant the rover should move into (tempX, coordinateY) contains an obstacle. In this case add the obstacle
	 * to the "encounteredObstacles" string and doesn't increases the value of coordinateX.
	 * Otherwise the method increases the value of coordinateX and doesn't add the obstacles to the "encounteredObstacles" string.
	 * This method implement the wrapping.
	 * 
	 * @return the "encounteredObstacles" string.
	 * 
	 * @throws MarsRoverException
	 */
	private String increasingX (String encounteredObstacles) throws MarsRoverException {
		
		int tempX = (this.coordinateX + 1) % this.planet.length;
		
		if (this.planetContainsObstacleAt(tempX, this.coordinateY)) {
			
			if (!encounteredObstacles.contains("(".concat(String.valueOf(tempX).concat(",".concat(String.valueOf(this.coordinateY)))))) {
				
				encounteredObstacles = encounteredObstacles.concat("(".concat(String.valueOf(tempX).concat(",".concat(String.valueOf(this.coordinateY).concat(")")))));
		
			}
		
		} else {
			
			this.coordinateX = tempX;
			
		}
		
		return encounteredObstacles;
		
	}
	
	/**
	 * It check if the quadrant the rover should move into (coordinateX, tempY) contains an obstacle. In this case add the obstacle
	 * to the "encounteredObstacles" string and doesn't increases the value of coordinateY.
	 * Otherwise the method increases the value of coordinateY and doesn't add the obstacles to the "encounteredObstacles" string.
	 * This method implement the wrapping.
	 * 
	 * @return the "encounteredObstacles" string.
	 * 
	 * @throws MarsRoverException
	 */
	private String increasingY (String encounteredObstacles) throws MarsRoverException {
		
		int tempY = (this.coordinateY + 1) % this.planet[0].length;
		
		if (this.planetContainsObstacleAt(coordinateX, tempY)) {
			
			if (!encounteredObstacles.contains("(".concat(String.valueOf(this.coordinateX).concat(",".concat(String.valueOf(tempY)))))) {
				
				encounteredObstacles = encounteredObstacles.concat("(".concat(String.valueOf(this.coordinateX).concat(",".concat(String.valueOf(tempY).concat(")")))));
			
			}
			
		} else {
			
			this.coordinateY = tempY;
			
		}
		
		return encounteredObstacles;
		
	}
	
	/**
	 * It check if the quadrant the rover should move into (tempX, coordinateY) contains an obstacle. In this case add the obstacle
	 * to the "encounteredObstacles" string and doesn't decreases the value of coordinateX.
	 * Otherwise the method decreases the value of coordinateX and doesn't add the obstacles to the "encounteredObstacles" string.
	 * This method implement the wrapping.
	 * 
	 * @return the "encounteredObstacles" string.
	 * 
	 * @throws MarsRoverException
	 */
	private String decreasingX (String encounteredObstacles) throws MarsRoverException {
		
		int tempX = (((this.coordinateX - 1) % this.planet.length) + this.planet.length) % this.planet.length;
		
		if (this.planetContainsObstacleAt(tempX, this.coordinateY)) {
			
			if (!encounteredObstacles.contains("(".concat(String.valueOf(tempX).concat(",".concat(String.valueOf(this.coordinateY)))))) {
				
				encounteredObstacles = encounteredObstacles.concat("(".concat(String.valueOf(tempX).concat(",".concat(String.valueOf(this.coordinateY).concat(")")))));
			
			}
		
		} else {
		
			this.coordinateX = tempX;

		}
		
		return encounteredObstacles;
		
	}
	
	/**
	 * It check if the quadrant the rover should move into (coordinateX, tempY) contains an obstacle. In this case add the obstacle
	 * to the "encounteredObstacles" string and doesn't decreases the value of coordinateY.
	 * Otherwise the method decreases the value of coordinateY and doesn't add the obstacles to the "encounteredObstacles" string.
	 * This method implement the wrapping.
	 * 
	 * @return the "encounteredObstacles" string.
	 * 
	 * @throws MarsRoverException
	 */
	private String decreasingY (String encounteredObstacles) throws MarsRoverException {
		
		int tempY = (((this.coordinateY - 1) % this.planet[0].length) + this.planet[0].length) % this.planet[0].length;
		
		if (this.planetContainsObstacleAt(this.coordinateX, tempY)) {
			
			if (!encounteredObstacles.contains("(".concat(String.valueOf(this.coordinateX).concat(",".concat(String.valueOf(tempY)))))) {
				
				encounteredObstacles = encounteredObstacles.concat("(".concat(String.valueOf(this.coordinateX).concat(",".concat(String.valueOf(tempY).concat(")")))));
			
			}
		
		} else {
		
			this.coordinateY = tempY;

		}
		
		return encounteredObstacles;
		
	}
	
	/**
	 * It checks if the obstacles exceeds the dimensions of the planet's grid and if the single coordinates
	 * values of his positions can be stored in a variable of "int" type.
	 * 
	 * @return <false> if an obstacle exceeds the planet's grid's limits, <true> otherwise.
	 * 
	 * @throws MarsRoverException
	 * @throws NumberFormatException
	 */
	private boolean checkPlanetObstacles() throws MarsRoverException {
	
		for (int i = 0; i < planetObstacles.size(); i++) {
			
			try {
				
				if (planetObstacles.get(i).indexOf(",") == -1 || planetObstacles.get(i).indexOf(")") == -1) {
					
					throw new MarsRoverException("The format of the position of the added obstacles is not correct.");
					
				} else if (Integer.parseInt(planetObstacles.get(i).substring(1, planetObstacles.get(i).indexOf(","))) > this.planet.length || Integer.parseInt(planetObstacles.get(i).substring(planetObstacles.get(i).indexOf(",")+1, planetObstacles.get(i).indexOf(")"))) > this.planet[0].length) {
					
					return false;
					
				}
				
			} catch (NumberFormatException e) {
				
				throw new NumberFormatException("The values of the coordinates of the obstacles cannot be stored in variables of 'int' type.");
				
			}
			
		}
		
		return true;
		
	}

}
