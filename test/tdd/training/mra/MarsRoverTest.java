package tdd.training.mra;

import static org.junit.Assert.*;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;

public class MarsRoverTest {
	
	MarsRover rover;
	List<String> planetObstacles;
	
	@Before
	public void setUp() throws MarsRoverException {
		
		planetObstacles = new ArrayList<>();
		
	}
	
	@Test
	// Test user story 1
	public void testDefinePlanetAndDetermineObstacles() throws MarsRoverException {
		
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		
		rover = new MarsRover(10, 10, planetObstacles);
		
		assertEquals(true, rover.planetContainsObstacleAt(2,3));
		assertEquals(true, rover.planetContainsObstacleAt(4,7));
		assertEquals(false, rover.planetContainsObstacleAt(5,6));
		
	}
	
	@Test
	// Test user story 2
	public void testLandingStatus() throws MarsRoverException {
		
		rover = new MarsRover(10, 10, planetObstacles);
		
		String commandString = "";
		
		assertEquals("(0,0,N)", rover.executeCommand(commandString));
	
	}
	
	@Test
	// Test user story 3
	public void testExcuteCommandR() throws MarsRoverException {
		
		rover = new MarsRover(10, 10, planetObstacles);
		
		String commandString = "r";
		
		assertEquals("(0,0,E)", rover.executeCommand(commandString));
	
	}
	
	@Test
	// Test user story 3
	public void testExcuteCommandL() throws MarsRoverException {
		
		rover = new MarsRover(10, 10, planetObstacles);
		
		String commandString = "l";
		
		assertEquals("(0,0,W)", rover.executeCommand(commandString));
	
	}
	
	@Test
	// Test user story 4
	public void testExcuteCommandF() throws MarsRoverException {
		
		rover = new MarsRover(10, 10, planetObstacles);
		
		String commandString = "f";
		
		assertEquals("(0,1,N)", rover.executeCommand(commandString));
	
	}
	
	@Test
	// Test user story 5, 7
	public void testExcuteCommandB() throws MarsRoverException {
		
		rover = new MarsRover(10, 10, planetObstacles);
		
		String commandString = "b";
		
		assertEquals("(0,9,N)", rover.executeCommand(commandString));
	
	}
	
	@Test
	// Test user story 6
	public void testExcuteCommandMovingCombined() throws MarsRoverException {
		
		rover = new MarsRover(10, 10, planetObstacles);
		
		String commandString = "ffrff";
		
		assertEquals("(2,2,E)", rover.executeCommand(commandString));
	
	}
	
	@Test
	// Test user story 8
	public void testExcuteCommandEncounterAnObstacle() throws MarsRoverException {
		
		planetObstacles.add("(2,2)");
		rover = new MarsRover(10, 10, planetObstacles);
		
		String commandString = "ffrfff";
		
		assertEquals("(1,2,E)(2,2)", rover.executeCommand(commandString));
	
	}
	
	@Test
	// Test user story 9
	public void testExcuteCommandEncounterMoreObstacles() throws MarsRoverException {
		
		planetObstacles.add("(2,2)");
		planetObstacles.add("(2,1)");
		rover = new MarsRover(10, 10, planetObstacles);
		
		String commandString = "ffrfffrflf";
		
		assertEquals("(1,1,E)(2,2)(2,1)", rover.executeCommand(commandString));
	
	}
	
	@Test
	// Test user story 10
	public void testExcuteCommandEncounterAnObstaclesAndWrapping() throws MarsRoverException {
		
		planetObstacles.add("(0,9)");
		rover = new MarsRover(10, 10, planetObstacles);
		
		String commandString = "b";
		
		assertEquals("(0,0,N)(0,9)", rover.executeCommand(commandString));
	
	}
	
	@Test
	// Test user story 11
	public void testExcuteCommandPlanetTour() throws MarsRoverException {
		
		planetObstacles.add("(2,2)");
		planetObstacles.add("(0,5)");
		planetObstacles.add("(5,0)");
		rover = new MarsRover(6, 6, planetObstacles);
		
		String commandString = "ffrfffrbbblllfrfrbbl";
		
		assertEquals("(0,0,N)(2,2)(0,5)(5,0)", rover.executeCommand(commandString));
	
	}
	
	// User Story 12
	// The grid of a planet has a limit. It is not possible to add obstacles that exceeds these limits.
	// Requirement: Implement MarsRover.MarsRover(int planetX, int planetY, List<String> planetObstacles) to throw a MarsRoverException
	// if an obstacle that exceeds planet's grid limits is added.	
	@Test (expected = MarsRoverException.class)
	// Test user story 12
	public void testAddingObstacleExceedsPlanetDimensions() throws MarsRoverException {
		
		planetObstacles.add("(11,2)");
		rover = new MarsRover(10, 10, planetObstacles);
	
	}
	
	// User Story 13
	// The position of an obstacles need to be expressed in this way "(x,y)".
	// Requirement: Implement MarsRover.checkPlanetObstacles() to throw a MarsRoverException
	// if the format of the position of the obstacle is not correct.
	@Test (expected = MarsRoverException.class)
	// Test user story 13
	public void testAddingObstacleWithIncorrectPositionFormat() throws MarsRoverException {
		
		planetObstacles.add("(1,2)");
		planetObstacles.add("+(2-");
		rover = new MarsRover(10, 10, planetObstacles);
	
	}
	
	// User Story 14
	// The single values of the coordinates of the position of an obstacles must be able to be stored in a variable of "int" type.
	// Requirement: Implement MarsRover.checkPlanetObstacles() to throw a NumberFormatException
	// if the value of the coordinate of an obstacle cannot be stored in a variable of "int" type.	
	@Test (expected = NumberFormatException.class)
	// Test user story 14
	public void testAddingObstacleWithIncorrectValueOfCoorinate() throws MarsRoverException {
		
		planetObstacles.add("(1,2)");
		planetObstacles.add("(two,2)");
		rover = new MarsRover(10, 10, planetObstacles);
	
	}
	
	// User Story 15
	// The command that the rover receives can be empty but if it contains characters it can
	// contain only one or more characters among the following characters: "f", "b", "r", "l".
	// Requirement: Implement MarsRover.executeCommand(String commandString) to throw a MarsRoverException
	// if the command is not correct.	
	@Test (expected = MarsRoverException.class)
	// Test user story 15
	public void testExcuteIncorrectCommand() throws MarsRoverException {
		
		rover = new MarsRover(10, 10, planetObstacles);
		
		String commandString = "f.4";
		
		rover.executeCommand(commandString);
	
	}

}
